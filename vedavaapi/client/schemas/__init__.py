import json
from copy import deepcopy

from vedavaapi.client import eprint

def _mfn(a, b, json_path=""):
    # assert a.__class__ == b.__class__, str(a.__class__) + " vs " + str(b.__class__)

    if isinstance(b, dict) and isinstance(a, dict):
        #if 'type' in b:
        #    return deepcopy(b)

        a_and_b = set(a.keys()) & set(b.keys())
        every_key = set(a.keys()) | set(b.keys())
        merged_dict = {}
        for k in every_key:
            if k in a_and_b:
                if k.startswith('_') and k != '_display':
                    merged_dict[k] = deepcopy(b[k])
                    continue
                merged_dict[k] = _mfn(a[k], b[k], json_path=json_path + "/" + k)
            else:
                if k in ['anyOf', 'oneOf'] and k in a and k not in b:
                    continue
                merged_dict[k] = deepcopy(a[k] if k in a else b[k])
        return merged_dict
    elif (isinstance(b, list)
            and isinstance(a, list)
            and not (json_path.endswith('jsonClass' + "/enum") or json_path.endswith('type' + "/enum"))):
        # TODO: What if we have a list of dicts?
        items_are_dicts = (len(a) and isinstance(a[0], dict)) or (len(b) and isinstance(b[0], dict))
        items_are_lists = (len(a) and isinstance(a[0], list)) or (len(b) and isinstance(b[0], list))
        return b if items_are_dicts else (a + b if items_are_lists else list(set(a + b)))
    else:
        return deepcopy(b)



class VVSchemas():

    def __init__(self, vc):
        super(VVSchemas, self).__init__()
        self.vc = vc
        self._cached_ids_to_classes_map = {}
        self._schemas_cache = {}

    def _get(self, _id=None, json_class_name=None):
        if not _id and not json_class_name:
            return None

        cached_json_class_name = None
        if _id and _id in self._cached_ids_to_classes_map:
            cached_json_class_name = self._cached_ids_to_classes_map[_id]
        elif json_class_name and json_class_name in self._schemas_cache:
            cached_json_class_name = json_class_name
        if cached_json_class_name:
            return self._schemas_cache[cached_json_class_name]

        selector_doc = {"jsonClass": "VVSchema"}
        if _id is not None:
            selector_doc['_id'] = _id
        else:
            selector_doc['name'] = json_class_name

        resp = self.vc.get('objstore/v1/resources', parms={"selector_doc": json.dumps(selector_doc)})
        eprint(resp.json())
        if resp.status_code != 200:
            return None

        matches = resp.json()
        if not len(matches['items']):
            return None

        json_class_def = matches['items'][0]

        self._schemas_cache[json_class_def['name']] = json_class_def
        self._cached_ids_to_classes_map[json_class_def['_id']] = json_class_def['name']
        return json_class_def

    def get(self, json_class_name):
        return self._get(json_class_name=json_class_name)


def recursively_merge_json_schemas(parent_class, schema, vv_schemas):
    if not vv_schemas:
        raise ValueError("remote_schemas should be provided for remote schemas")

    schema = deepcopy(schema)
    parent_schema = vv_schemas.get(parent_class)
    if not parent_schema:
        raise ValueError('parent schema not present on remote')

    for k in ('properties', 'required', '_display', 'type'):
        if k in schema and k not in parent_schema:
            continue
        if k not in schema and k not in parent_schema:
            schema[k] = parent_schema
        if k in schema and k in parent_schema:
            schema[k] = _mfn(schema[k], parent_schema[k], json_path=k)

    return schema
