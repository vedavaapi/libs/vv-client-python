import csv
import re, string
from functools import reduce

from vedavaapi.client import eprint
from vedavaapi.client.objstore import post_graph

token_re = re.compile(r'([^\s।॥{punct}]*)([\s।॥{punct}]*)'.format(punct=re.escape(string.punctuation)))

trigger_str = '>'

vakya_start_trigger = '<>'
vakya_end_trigger = '</>'


def get_section_res(source, index, name, section_type):
    section = {
        "jsonClass": "BookSection",
        "source": source,
        "selector": {
            "jsonClass": "IndexSelector",
            "index": index
        },
        "jsonClassLabel": section_type
    }
    if name is not None:
        section['name'] = {
            "jsonClass": "Text",
            "chars": name
        }
    return section


def get_members_selector(resource_ids=None, field_filter=None, index_field=None, item_indices=None, ranges=None):
    members_selector = {
        "jsonClass": "MembersSelector"
    }
    if isinstance(resource_ids, list):
        members_selector['resource_ids'] = resource_ids
    if isinstance(field_filter, dict):
        field_filter = field_filter.copy()
        if 'jsonClass' in field_filter:
            field_filter['jsonClassName'] = field_filter.pop('jsonClass')
        members_selector['field_filter'] = field_filter
    if index_field and (isinstance(item_indices, list) or isinstance(ranges, list)):
        index_filter = {
            "index_field": index_field
        }
        if isinstance(item_indices, list) and len(item_indices):
            index_filter['indices'] = item_indices
        if isinstance(ranges, list) and len(ranges):
            range_docs = [{"from": r[0], "to": r[1]} for r in ranges if r and len(r) == 2]
            if len(range_docs):
                index_filter['range'] = range_docs
        if 'indices' in index_filter or 'range' in index_filter:
            members_selector['index_filters'] = [index_filter]
    return members_selector


def tokenize_text(text):
    token_groups = token_re.findall(text)
    tokens = []
    for tg in token_groups:
        tokens.append(tg[0])
        if tg[1]:
            tokens.append(tg[1])
    return tokens


def get_sp_res_blank_id(index):
    return "_:sp-{}".format(index)

def get_sp_res(book_id, index, text, is_delimiter):
    return {
        "jsonClass": "SamyuktaPada",
        "_id": get_sp_res_blank_id(index),
        "source": book_id,
        "selector": {
            "jsonClass": "IndexSelector",
            "index": index
        },
        "text": {
            "jsonClass": "Text",
            "chars": text
        },
        "is_delimiter": is_delimiter
    }


def handle_samyukta_padas(current_sp_index, paada_text, book_id, sp_graph, vakya_bounds, sp_buffer, vc=None, is_last_row=False):
    token_groups = token_re.findall(paada_text)
    count = 0
    for tg in token_groups:
        if tg[0]:
            count += 1
            sp_res = get_sp_res(book_id, current_sp_index + count, tg[0], False)
            sp_buffer.append(sp_res)
        if tg[1]:
            count += 1
            if vakya_start_trigger in tg[1]:
                if not len(vakya_bounds) or len(vakya_bounds[-1]) == 2:
                    vakya_bounds.append([current_sp_index + count + 1])
                else:
                    vakya_bounds[-1][0] = count + 1
            elif vakya_end_trigger in tg[1]:
                if not len(vakya_bounds) or len(vakya_bounds[-1]) == 2:
                    pass
                else:
                    vakya_bounds[-1].append(current_sp_index + count - 1)
            sp = str(tg[1]).replace('<>', '').replace('</>', '')
            sp_res = get_sp_res(book_id, current_sp_index + count, sp, True)
            sp_buffer.append(sp_res)

    if len(sp_buffer) >= 100 or is_last_row:
        sp_buffer_graph = dict((sp['_id'], sp) for sp in sp_buffer)
        if not vc:
            sp_graph.update(sp_buffer_graph)
        else:
            response = post_graph(vc, sp_buffer_graph, should_return_resources=False, should_return_oold_resources=False)
            sp_graph.update(response['graph'])
        sp_buffer.clear()
        eprint(current_sp_index)

    return count


def import_from_rows(book_id, rows, vc=None):
    if not len(rows):
        return {}
    headers_row = rows[0]
    depth = len(headers_row)
    depth_range = range(0, depth)

    sp_graph = {}
    hierarchy = {}

    vakya_bounds = []

    sp_buffer = []
    section_indices = [0] * depth
    prev_resolved_row = [''] * depth

    current_sp_index = 0

    for i, row in enumerate(rows[1:]):
        eprint({"row": i})
        is_last_row = (i == len(rows) - 2)
        row = row + [''] * (depth - len(row))
        resolved_row = row[:]

        prev_level_changed = False
        for j in depth_range:
            if j != depth - 1 and not prev_level_changed and (not row[j] or (row[j] == prev_resolved_row[j] and row[j] != trigger_str)):
                resolved_row[j] = prev_resolved_row[j]
            else:
                resolved_row[j] = row[j]
                section_indices[j] = (section_indices[j] + 1) if not prev_level_changed else 1
                prev_level_changed = True
        # eprint(prev_resolved_row, row, resolved_row)
        prev_resolved_row = resolved_row[:]

        for j in depth_range:
            blank_id = '_:d{d}-{index}'.format(d=j, index='-'.join([str(ind) for ind in section_indices[:j+1]]))
            source_id = book_id if (j == 0) else '_:d{d}-{index}'.format(d=j-1, index='-'.join([str(ind) for ind in section_indices[:j]]))

            section_resource = get_section_res(
                source_id, section_indices[j],
                resolved_row[j] if (resolved_row[j] != trigger_str and j != depth -1 ) else None, headers_row[j]
            )
            section_resource['_id'] = blank_id

            if j == depth - 1:
                section_resource['jsonClass'] = 'Sequence'
                paada_text = resolved_row[j] + ('' if str(resolved_row[j]).endswith('\n') else '\n')
                sp_count = handle_samyukta_padas(
                    current_sp_index, paada_text, book_id, sp_graph, vakya_bounds, sp_buffer, vc=vc, is_last_row=is_last_row)

                members_selector = get_members_selector(
                    resource_ids=[get_sp_res_blank_id(current_sp_index + 1 + l) for l in range(0, sp_count)],
                    field_filter={"jsonClassName": "SamyuktaPada", "source": book_id},
                    index_field='selector.index', ranges=[[current_sp_index + 1, current_sp_index + sp_count]]
                )
                section_resource["members"] = members_selector

                current_sp_index += sp_count

            hierarchy[blank_id] = section_resource

    eprint(vakya_bounds)
    if len(vakya_bounds) and len(vakya_bounds[-1]) < 2:
        vakya_bounds.pop(len(vakya_bounds) - 1)

    return {
        "hierarchy": hierarchy,
        "sp_graph": sp_graph,
        "vakya_bounds": vakya_bounds,
        "section_indices": section_indices,
        "headers": headers_row,
        "sp_count": current_sp_index
    }


def set_resolved_sp_ids(graph, sp_id_uid_graph):
    for item in graph.values():
        if not item.get('members', {}).get('resource_ids', []):
            continue
        resource_ids = item['members']['resource_ids']
        for i, rid in enumerate(resource_ids):
            resource_ids[i] = sp_id_uid_graph[rid]


def get_vakyas(book_id, vakya_bounds):
    vakya_graph = {}
    for i, vb in enumerate(vakya_bounds):
        vakya = get_section_res(book_id, i, None, 'Vakya')
        resource_ids = [get_sp_res_blank_id(j) for j in range(vb[0], vb[1] + 1)]
        members_selector = get_members_selector(
            resource_ids=resource_ids, field_filter={"jsonClassName": "SamyuktaPada", "source": book_id}, index_field='selector.index', ranges=[vb])
        vakya['members'] = members_selector
        vakya_blank_id = '_:vakya-{}'.format(i)
        vakya['_id'] = vakya_blank_id
        vakya_graph[vakya_blank_id] = vakya
    return vakya_graph


def import_from_csv(vc, book_id, file):
    rows = list(csv.reader(open(file)))

    result = import_from_rows(book_id, rows, vc=vc)
    hierarchy = result['hierarchy']
    set_resolved_sp_ids(hierarchy, result['sp_graph'])

    hierarchy_post_response = post_graph(vc, hierarchy, should_return_resources=False)

    vakya_graph = get_vakyas(book_id, result['vakya_bounds'])
    set_resolved_sp_ids(vakya_graph, result['sp_graph'])
    vakyas_post_response = post_graph(vc, vakya_graph, should_return_resources=False)

    return {
        "hierarchy": hierarchy_post_response,
        "vakyas": vakyas_post_response
    }


def post_book(vc, library, title):
    book = {
        "_id": "_:book",
        "jsonClass": "BookPortion",
        "source": library,
        "title": [
            {
                "jsonClass": "Text",
                "chars": title
            }
        ]
    }

    resp = post_graph(vc, {"_:book": book})
    return resp['graph']['_:book']


def graph_to_csv(vc, book_id, file):
    pass


'''
from vedavaapi.client import VedavaapiSession
vs = VedavaapiSession('https://apps.vedavaapi.org/prod/api')
# vs.signin('info@vedavaapi.org', '1234')
vs.access_token = '64y3NS0RMg4GP9qVnXbRjkkYZ2MVtPf1og7Ab8qfJp'
from vedavaapi.client import book_csv_importer as bci
book = bci.post_book(vs, '0', 'AbhidhaanaManjarii')
r = bci.import_from_csv(vs, book['_id'], '/home/damodarreddy/పొందినవి/గురుకులం/pgr/abhidhanamanjarii/a2.csv')
'''
