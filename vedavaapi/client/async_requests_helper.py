import aiohttp
import asyncio


async def fetch_json(session: aiohttp.ClientSession, url: str, set_progress):
    async with session.get(url) as response:
        try:
            response.raise_for_status()
        except aiohttp.ClientResponseError as e:
            return None
        set_progress()
        return await response.json()


async def get_data_asynchronous(urls, update_state=None):
    fetched_count = 0

    if update_state:
        task_id = update_state.__self__.request.id if update_state else None

    def set_progress():
        nonlocal fetched_count
        fetched_count += 1
        print(fetched_count)
        if not update_state:
            return
        #  eprint(update_state, update_state.__self__.request.id)
        update_state(task_id=task_id, state='PROGRESS', meta={"total": len(urls), "current": fetched_count, "status": "getting page details..."})

    async with aiohttp.ClientSession() as session:
        coroutines = await asyncio.gather(
            *[fetch_json(session, url, set_progress) for url in urls])

    return coroutines


def get_urls(urls, update_state=None):
    return asyncio.get_event_loop().run_until_complete(
        get_data_asynchronous(urls, update_state=update_state))
