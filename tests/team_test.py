#!/usr/bin/python
import sys
import os

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

from vedavaapi.client import *

if len(sys.argv) < 3:
    print("Usage: {} <site_url> <email> <password>".format(fname))
    sys.exit(1)

site_url = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]
vsession = VedavaapiSession(site_url)

print("Signing in as admin user {} ...".format(email))
vsession.signin(email, password)

print("creating a team ...")
team1 = accounts.create_team(vsession, "team 1", "", None)
print("team1 created: ", team1["_id"])

user1 = accounts.find_user(vsession, "user1@gmail.com")
if user1:
    print("deleting existing user ...")
    delusers = accounts.delete_users(vsession, [user1['_id']])

print ("creating afresh user1")
user1 = accounts.create_user(vsession, "user1@gmail.com", "1234", "sai")
print("user1 created: ", user1['_id'])

print("granting 'updateContent' of team1's members to user1 ...")
resp = vsession.post('/acls/v1/{}'.format(team1['_id']), data={
        "actions": json.dumps(["updateContent"]),
        "control": "grant",
        "user_ids": json.dumps([user1['_id']]) })
resp.raise_for_status()

print("Now logging in as user1 ...")
vsession_user1 = VedavaapiSession(site_url)

vsession_user1.signin(user1['email'], "1234")

user2 = accounts.find_user(vsession, "user4@gmail.com")
if user2:
    delusers = accounts.delete_users(vsession, [user1['_id']])
user2 = accounts.create_user(vsession, "user4@gmail.com", "2234", "rama")
print("user2 created: ", user2['_id'])

print("As user1, adding user2 to team ...")
resp = vsession_user1.post('/accounts/v1/teams/{}/members'.format(team1['_id']),
    data = { 'member_identifiers' : json.dumps([user2['_id']])})
print("resp: ", resp.json())
resp.raise_for_status()
print(resp.json())
