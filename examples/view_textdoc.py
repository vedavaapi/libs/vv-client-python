#!/usr/bin/python
import sys
import os
from pprint import pprint
import networkx as nx
import pydot
from io import StringIO
import re

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

from vedavaapi.client import *

if len(sys.argv) < 3:
    print("Usage: {} <site_url> <email> <password>".format(fname))
    sys.exit(1)


site_url = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]
textdoc_id = sys.argv[4] if len(sys.argv) > 4 else None
vsession = VedavaapiSession(site_url)

vsession.signin(email, password)

# Lookup the textdocs service
resp = vsession.get('/objstore/v1/resources', 
    parms={'selector_doc' : json.dumps({'jsonClass': 'Service', 
        'apis' : '/from_markup' })})
resp.raise_for_status()
if not len(resp.json()['items']):
    print("Cannot find Text Docs Service");
    # sys.exit(1)
    textdocs_url = 'http://localhost:5001'
else:
    svc = resp.json()['items'][0]
    textdocs_url = svc['url']

print("Enter a textdocument markup: ")
textdoc_markup = sys.stdin.read()

if not textdoc_id:
    print("Post a sample TextDocument")
    resp = requests.post("{}/from_markup".format(textdocs_url),
        data={"site_url" : vsession.base_url, 
            "access_token" : vsession.access_token, "markup" : textdoc_markup })
    pprint(resp.json())
    resp.raise_for_status()
    textdoc_id = resp.json()['root_text_doc_id']

print("Retrieving a TextDocument by id")
resp = vsession.get('objstore/v1/graph', 
    parms={'start_nodes_selector' : json.dumps({'_id' : textdoc_id}), 
    'max_hops' : -1, 'attach_in_links' : 'false', 'direction' : 'referrer',
    'json_class_projection_map' : json.dumps({'*' : {'resolvedPermissions' : 0}}) })
resp.raise_for_status()
textdoc_graph = resp.json()
#print(json.dumps(textdoc_graph, indent=2, ensure_ascii=False))
textdoc_tree = resp.json()['dot_graph']
with open("x.dot", "w") as f:
    f.write(textdoc_tree)

for node_id in textdoc_graph['graph_order']:
    n = textdoc_graph['graph'][node_id]
    if 'content' in n and n.get('jsonClassLabel') == 'vaakya':
        print("{}: {}".format(n['jsonClassLabel'], n['_id']))
        print("\n".join(re.split(r'[।॥\|]', n['content'])))

sys.exit(0)
