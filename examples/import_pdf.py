#!/usr/bin/python
'''
ex1: python3 examples/import_pdf.py -a 'https://r1.bharatikosha.org/api' -e 'info@vedavaapi.org' -p '1234' -l '5f468ee92ab17a000782dab7' -f '/path/to/pdf.pdf'

ex2: python3 examples/import_pdf.py -a 'https://r1.bharatikosha.org/api' -e 'info@vedavaapi.org' -p '1234' -l '5f468ee92ab17a000782dab7' -u "https://archive.org/download/dli.granth.16966/16966.pdf"
'''
import argparse
import json
import sys
import os
from typing import Optional

import requests

(dir_path, fname) = os.path.split(__file__)
app_path = dir_path
if app_path:
    os.chdir(app_path)
sys.path.insert(0, os.path.join(app_path, ".."))

from vedavaapi.client import VedavaapiSession


def set_repr(book, pdf_oold):
    """
    We will say pdf as a repr of book. it doesn't add any role in import flow.
    But it keeps both linked symantically, and helps in ui
    """
    repr = {
        "jsonClass": "StillImageRepresentation",
        "data": f'_OOLD:{pdf_oold["_id"]}',  # we use '_OOLD:' to refer blank doc in oold graph.
        "mimetype": 'application/pdf',
    }
    book['represntations'] = {
        "jsonClass": "DataRepresentations",
        "stillImage": [repr],  # pdf is a StillImage OOLD
        "default": "stillImage"
    }


def compute_book_and_pdf(
        library_id: str,
        file_path: Optional[str] = None, url: Optional[str] = None,
):
    """
  It computes book and pdf oold docs.
  We can source basic book metadata from other places too.
  """
    if not url and not file_path:
        raise ValueError('Any of url or file_path is required')
    proto = 'file' if file_path else 'http'
    title = os.path.basename(url or file_path).rsplit('.', 1)[0]  # sourcing book title from file name for simplicity.

    # It will go into 'graph'
    book = {
        "_id": f'_:book',  # blank graph id
        "jsonClass": "ScannedBook",
        "title": title,
        "source": library_id,
    }

    # It will go into 'oold_graph'
    pdf_oold = {
        "_id": "_:pdf-1",  # unique blank graph id
        "jsonClass": "StillImage",  # dctypes:stillImage
        "source": f'_VV:{book["_id"]}',
        # We are making it as child of book. So that When book get's deleted, it will also be deleted. we use '_VV:' to refer blank_id in main 'graph'.
        "namespace": "_vedavaapi"  # one of '_vedavaapi', '_web', '_abstract'. Here it willl be physical local file.
    }

    if proto == 'file':
        pdf_oold['identifier'] = os.path.basename(file_path)
    else:
        pdf_oold['identifier'] = url
        pdf_oold['proto'] = proto

    set_repr(book, pdf_oold)  # see comment for method.
    return book, pdf_oold


def create_book_and_pdf(
        vc: VedavaapiSession, library_id: str,
        file_path: Optional[str] = None, url: Optional[str] = None,
):
    book, pdf_oold = compute_book_and_pdf(library_id, file_path=file_path, url=url)
    graph = {book['_id']: book}
    oold_graph = {pdf_oold['_id']: pdf_oold}
    files = {"files": open(file_path, 'rb')} if file_path else None

    post_data = {
        "graph": graph, "ool_data_graph": oold_graph,
    }
    for k in post_data:
        post_data[k] = json.dumps(post_data[k])

    resp = vc.post('objstore/v1/graph', data=post_data, files=files)
    #  print(resp.json())
    resp.raise_for_status()

    result = resp.json()
    created_book_id = result['graph'][book['_id']]
    created_pdf_id = result['ool_data_graph'][pdf_oold['_id']]

    return created_book_id, created_pdf_id


def import_pages_from_pdf(
        vc: VedavaapiSession, book_id: str, pdf_id: str,
        is_scanned=True, pdffs_args=None, set_cover=True
):
    post_data = {
        "book_id": book_id,
        "pdf_id": pdf_id,
        "is_scanned_pdf": json.dumps(is_scanned),
        "set_cover": json.dumps(set_cover)
    }
    if pdffs_args is not None:
        post_data['pdffs_args'] = pdffs_args
    resp = vc.post('importer/v1/pdf_importer/import', data=post_data)
    #  print(resp.json())
    resp.raise_for_status()
    return resp.json()


def import_pdf(
        vc: VedavaapiSession, library_id: str,
        file_path: Optional[str] = None, url: Optional[str] = None,
        is_scanned=True, pdffs_args=None, set_cover=True
):
    print('1: creating book and pdf')
    book_id, pdf_id = create_book_and_pdf(vc, library_id, file_path=file_path, url=url)
    print('2: importing pages from pdf')
    import_status = import_pages_from_pdf(
        vc, book_id, pdf_id,
        is_scanned=is_scanned, pdffs_args=pdffs_args, set_cover=set_cover
    )
    return book_id, pdf_id, import_status


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--api', type=str, required=True)
    parser.add_argument('-e', '--email', type=str, required=True)
    parser.add_argument('-p', '--pwd', type=str, required=True)
    parser.add_argument('-l', '--lib', type=str, required=True)
    parser.add_argument('-f', '--file', type=str, required=False)
    parser.add_argument('-u', '--url', type=str, required=False)
    parser.add_argument(
      '-s', '--scanned', type=bool, required=False, default=True)

    args, _ = parser.parse_known_args()

    # We will set zoom to optimal if pdf is not scanned. recommended
    pdffs_args = None if args.scanned else 'zoom=optimal'

    vc = VedavaapiSession(args.api)
    vc.signin(args.email, args.pwd)

    book_id = pdf_id = import_status = None
    try:
        book_id, pdf_id, import_status = import_pdf(
          vc, args.lib, file_path=args.file, url=args.url,
          is_scanned=args.scanned, pdffs_args=pdffs_args, set_cover=True
        )
    except requests.HTTPError as e:
        print('error in importing', e.response.json())
        sys.exit(1)

    print('Import success.')
    print(json.dumps({
      "book_id": book_id,
      "pdf_id": pdf_id,
      "seq_id": import_status['seq_id'],
      "page_count": import_status['page_count']
    }, indent=4))
