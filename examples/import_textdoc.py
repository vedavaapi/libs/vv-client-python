#!/usr/bin/python
import sys
import os
from pprint import pprint

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

from vedavaapi.client import *

if len(sys.argv) < 3:
    print("Usage: {} <site_url> <email> <password>".format(fname))
    sys.exit(1)


site_url = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]
vsession = VedavaapiSession(site_url)

vsession.signin(email, password)

TextDocDef = {
      "jsonClass": "JsonClassDef",
      "name": "MyTextDocument",
      "schema": {
        "_display": [
          "",
          "jsonClass",
          "name",
          "content"
        ],
        "_primary_keys": [],
        "properties": {
          "_id": {
            "description": "unique id of the object",
            "type": "string"
          },
          "jsonClass": {
            "default": "Resource",
            "enum": [
              "MyTextDocument"
            ],
            "title": "jsonClass"
          },
          "jsonClassLabel": {
            "title": "label for json class.",
            "type": "string"
          },
          "name": {
            "type": "string"
          },
          "content": {
            "type": "string"
          },
          "source" : {
            "type": "string"
          }
        },
        "required": [
          "jsonClass",
          "content"
        ],
        "type": "object"
      },
#      "source": "5cf254a0b7a4fd0009ad231b"
    }

print("Creating new schema for TextDocument")
resp = vsession.post('objstore/v1/resources', data={'resource_jsons': json.dumps([TextDocDef]), 'upsert' : 'true'})
print(resp.json())
resp.raise_for_status()

print("Inserting a new TextDocument")
mytextdoc = {
	'jsonClass' : 'MyTextDocument',
	'name' : 'My Sample Book'
}

x = input("Type the text to import: ")

mytextdoc['content'] = x

resp = vsession.post('objstore/v1/resources', data={'resource_jsons': json.dumps([mytextdoc]), 'upsert': 'true'})
print(resp.json())
resp.raise_for_status()
myid = resp.json()[0]['_id']
print(myid)

print("Retrieving the TextDocument back")
resp = vsession.get('objstore/v1/resources/{}'.format(myid))
print(resp.json()['content'])
resp.raise_for_status()

print("Inserting tokenized words into TextDocument hierarchy")
words = x.split()
wordobjs = [{'content' : w, 'source' : myid, 'jsonClass' : 'MyTextDocument'}
    for w in words]
resp = vsession.post('objstore/v1/resources', data={'resource_jsons': json.dumps(wordobjs), 'upsert': 'true'})
pprint(resp.json())
resp.raise_for_status()

print("Creating a sequence of the words")
seq = {'jsonClass' : 'SequenceAnnotation', 
       'jsonClassLabel' : 'WordSequence',
       'target' : myid, 
       'body' : { 'members' : [
                    {'jsonClass' : 'WrapperObject', 'index' : i, 'resource' : r['_id']}
                    for i, r in enumerate(resp.json()) ],
               'jsonClass' : 'WrapperObject' 
             }
      }
resp = vsession.post('objstore/v1/resources', data={'resource_jsons': json.dumps([seq]), 'upsert': 'true'})
pprint(resp.json())
resp.raise_for_status()

resp = vsession.get('objstore/v1/resources', 
        parms={'selector_doc' : json.dumps({ 
                    'jsonClass' : 'SequenceAnnotation', 
                    'jsonClassLabel' : 'WordSequence', 
                    'target' : myid})

             })
pprint(resp.json())
resp.raise_for_status()
wordseq = resp.json()['items'][0]['body']['members']
wordids = [w['resource'] for w in wordseq]
word2idx = dict((w['resource'], w['index']) for w in wordseq)

resp = vsession.get('objstore/v1/resources', 
        parms={'selector_doc' : json.dumps({ 
                    'source' : myid, 'jsonClass' : 'MyTextDocument'}),
               'projection' : json.dumps({'content' : 1, '_id': 1}),
               'sort_doc' : json.dumps([['content', 1]])
              }
             )
pprint(resp.json())
resp.raise_for_status()
words = sorted(resp.json()['items'], key = lambda x: word2idx[x['_id']])
pprint(words)
