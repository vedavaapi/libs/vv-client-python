#!/usr/bin/python
import sys
import os

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

from vedavaapi.client import *

if len(sys.argv) < 3:
    print("Usage: {} <site_url> <email> <password>".format(fname))
    sys.exit(1)


site_url = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]
vsession = VedavaapiSession(site_url)

vsession.signin(email, password)

def cleanup_svc_registry(vc):
    parms = {
        "selector_doc" : json.dumps({'jsonClass': "Service"}),
        "projection" : json.dumps({'_id' : 1})
    }
    svcs = vc.get('objstore/v1/resources', parms=parms).json()
    print(svcs)
    svc_ids = [svc['_id'] for svc in svcs['items']]
    print("Deleting services with ids {}".format(svc_ids))
    vc.delete('objstore/v1/resources',
        data={'resource_ids' : json.dumps(svc_ids)})

def register_svc(vsession, svc_obj):
    svc_obj['jsonClass'] = "Service"
    parms = {
        "resource_jsons": json.dumps([svc_obj]),
        "return_projection": '{"permissions": 0}',
        "upsert": "true"
    }
    r = vsession.post('objstore/v1/resources', data=parms)
    svc_id = r.json()[0]['_id']
    print("Giving world read permission to ", svc_obj['name'])
    r = vsession.post('/acls/v1/{}'.format(svc_id),
        data={ 'actions' : '["read"]', 'control' : 'grant',
                'user_ids' : '["*"]' })
    r.raise_for_status()

base_url = "https://apps.vedavaapi.org/stage/apps/"

cleanup_svc_registry(vsession)

register_svc(vsession, {
        "name" : "Word Segmenter",
        "url" : base_url+"segmenter", 
        "apis" : ["/segments", "/bulk_segment"],
        "handledTypes" : ["ScannedPage"] })

register_svc(vsession, {
        "name" : "IIIT LDetector",
        "url" : base_url+"ldetector",
        "apis" : ["/segments", "/bulk_segment"],
        "handledTypes" : ["ScannedPage"] })

register_svc(vsession, {
        "name" : "Mirador",
        "url" : base_url+"mirador/index.html",
        "handledTypes" : ["ScannedBook"]})

register_svc(vsession, {
        "name" : "OCR ProofReader",
        "url" : base_url+"ocreditor/files/index.html",
        "handledTypes" : ["ScannedPage"] })

register_svc(vsession, {
        "name" : "Tesseract OCR",
        "url" : base_url+"tessocr",
        "apis" : ["/segments", "/bulk_segment"],
        "handledTypes" : ["ScannedPage"] })

register_svc(vsession, {
        "name" : "Google OCR",
        "url" : base_url+"googleocr",
        "apis" : ["/segments", "/bulk_segment"],
        "handledTypes" : ["ScannedPage"] })

register_svc(vsession, {
        "name" : "Text Docs",
        "url" : base_url+"textdocs",
        "apis" : ["/from_markup", "/from_scanned_book", "/scanned_pages_text"],
        "handledTypes" : ["ScannedPage"] })

register_svc(vsession, {
        "name" : "TextDocs Editor",
        "url" : base_url+"textdocs/files/index.html",
        "handledTypes" : ["TextDocument"] })
