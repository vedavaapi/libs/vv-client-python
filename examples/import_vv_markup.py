import re
import sys
from pprint import pprint
from functools import reduce

tokens_re = re.compile(r'\[.*?\]|[^\[\]]*')

text = sys.stdin.read()

tokens = tokens_re.findall(text)

class layout_generator:
    context_stack = []
    tags_db = { }
    layout = []
        
    # [sh [pa] label=x script=y]
    @classmethod
    def parse_tag(cls, t):
        text = re.match(r'^\[(.*?)\]', t)
        if not text:
            return None
        #print(text.group(1))
        components = text.group(1).split()
        tag = components[0]

        dict = {}
        if tag.startswith('/'):
            tag = tag.lstrip('/')
            dict['end'] = True
        dict['tag'] = tag
        for c in components:
            if '=' in c:
                (k, v) = c.split('=')
                if ',' in v:
                    v = v.split(',')
                dict[k] = v
        #print(dict)
        return dict

    @classmethod
    def pop_context(cls):
        deltag = cls.context_stack.pop()
        if cls.context_stack:
            cls.context_stack[-1]['content'].append(deltag['content'])
        else:
            cls.layout.append(deltag['content'])

    @classmethod
    def update_context(cls, t):
        if 'end' in t:
            tag_found = reduce(
                lambda x,y : x or y,
                map(lambda x: t['tag'] == x, cls.context_stack))
            if not tag_found:
                # Spurious end tag; ignore
                return
            # Else, pop out all tags until the matching tag in stack
            while cls.context_stack and cls.context_stack[-1]['tag'] != t['tag']:
                cls.pop_context()
            if cls.context_stack:
                cls.pop_context()
            return
                
        if not cls.context_stack:
            cls.context_stack.append(t)
            return

        if cls.context_stack[-1]['tag'] == 'vv_markup':
            return

        # If new child context, push it onto stack
        if cls.child_of(t['tag'], cls.context_stack[-1]) or \
                cls.attr_of(t['tag'], cls.context_stack[-1]):
            cls.context_stack.append(t)
            return

        newt = t.copy()
        newt['end'] = True
        cls.update_context(newt)
        cls.context_stack.append(t)

    @classmethod
    def cur_context(cls):
        return cls.context_stack[-1] if cls.context_stack else None

    @classmethod
    def build_tag_relations(cls):
        pass

    @classmethod
    def child_of(cls, a, b):
        pass

    @classmethod
    def attr_of(cls, a, b):
        pass

    @classmethod
    def process_tag(cls, t):
        newtag = cls.parse_tag(t)
        #print(newtag)
        cls.update_context(newtag)

        # Handle the markup description section
        if newtag['tag'] == 'vv_markup':
            if 'end' in newtag:
                cls.build_tag_relations()
            return
        if cls.cur_context() == 'vv_markup':
            cls.tags_db[newtag['tag']] = newtag

        # Handle a tag


    @classmethod
    def process_text(cls, t):
        ctx = cls.cur_context()
        txt_elem = {'class' : '_', 'content' : t}
        if ctx:
            if not 'content' in ctx:
                ctx['content'] = []
            ctx['content'].append(txt_elem)
        else:
            cls.layout.append(txt_elem)

#print(tokens)
for t in tokens:
    if t.startswith('['):
        layout_generator.process_tag(t)
    else:
        layout_generator.process_text(t)
print(layout_generator.layout)
