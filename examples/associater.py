import json
import os
import re
import sys

from requests import HTTPError

(dir_path, fname) = os.path.split(__file__)
app_path = dir_path
if app_path:
    os.chdir(app_path)
sys.path.insert(0, os.path.join(app_path, ".."))

from vedavaapi.client import VedavaapiSession, objstore, ObjModelException


def get_page_ids(vc: VedavaapiSession, book_id):
    if book_id is None:
        return []

    seq_anno_resp = vc.get(
        'objstore/v1/resources',
        parms={
            "selector_doc": json.dumps(
                {"jsonClass": "SequenceAnnotation", "target": book_id, "canonical": "default_canvas_sequence"}),
            "projection": json.dumps({"jsonClass": 1, "_id": 1, "body": 1})}
    )
    if seq_anno_resp.status_code == 200 and seq_anno_resp.json()['items']:
        seq_anno = seq_anno_resp.json()['items'][0]
        seq_anno_members = seq_anno.get('body', {}).get('members', [])
        sequenced_ids = [m['resource'] for m in seq_anno_members]
        return sequenced_ids

    selector_doc = {"jsonClass": "ScannedPage", "source": book_id}
    projection = {"selector": 1, "index": 1, "_id": 1}
    pages_resp = vc.get(
        'objstore/v1/resources',
        parms={"selector_doc": json.dumps(selector_doc), "projection": json.dumps(projection)})
    try:
        pages_resp.raise_for_status()
    except HTTPError as e:
        raise e

    pages = pages_resp.json()['items']
    return [p['_id'] for p in pages]


def associate(page_id, graph: dict, page_lines: list):
    #  print(page_id, page_lines)
    page = graph.get(page_id)
    if page is None:
        return None

    associations = []
    reached_ids = page.get('_reached_ids')
    if not reached_ids or not reached_ids.get('source'):
        return associations

    regions = []
    for rid in reached_ids['source']:
        region = graph.get(rid)
        if not region:
            continue
        if not region.get('selector', {}).get('default', {}).get('value'):
            continue
        regions.append(region)

    def _sort_key_fn(_region):
        fragment_selector = _region['selector']['default']
        fragment_val = fragment_selector['value']
        fragment_val_regex = r'xywh=(?P<x>[0-9]+),(?P<y>[0-9]+),(?P<w>[0-9]+),(?P<h>[0-9]+)'
        x, y, w, h = re.match(fragment_val_regex, fragment_val).groups()
        return int(y), int(x)

    sorted_regions = sorted(regions, key=_sort_key_fn)

    associations_count = min(len(regions), len(page_lines))

    for i, region in enumerate(sorted_regions[:associations_count]):
        anno_ids = region.get('_reached_ids', {}).get('target', [])
        associated_anno_id = anno_ids[0] if len(anno_ids) else '_:{}-anno'.format(region['_id'])
        anno = {
            "jsonClass": "TextAnnotation",
            "_id": associated_anno_id,
            "body": [{
                "jsonClass": "Text",
                "chars": page_lines[i]
            }],
            "target": region['_id'],
            "generator": "Associater"
        }
        associations.append((region, anno))

    return associations


def get_associations_graph(vc: VedavaapiSession, page_ids, page_line_collns):

    start_nodes_selector = {"jsonClass": "ScannedPage", "_id": {"$in": page_ids}}

    traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "ImageRegion"}},
        {"target": {"jsonClass": "TextAnnotation"}}
    ]
    direction = 'referrer'
    max_hops = 2
    json_class_projection_map = {"*": {"resolvedPermissions": 0}}

    try:
        print('getting regions graph')
        regions_graph_response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction=direction, max_hops=max_hops,
            json_class_projection_map=json_class_projection_map
        )
        regions_graph = regions_graph_response['graph']
        print('regions graph retrieved')
    except HTTPError as e:
        raise ObjModelException(
            'error in getting regions graph',
            status_code=e.response.status_code, attachments={"error": str(e)}
        )

    associations_graph = {}
    pages_count = min(len(page_ids), len(page_line_collns))

    for index, page_id in enumerate(page_ids[:pages_count]):
        associations = associate(page_id, regions_graph, page_line_collns[index])
        if associations is None:
            continue
        associations_graph.update(dict((anno['_id'], anno) for (region, anno) in associations))

    return associations_graph


def post_associations(vc: VedavaapiSession, book_id, lines_stream):
    print('getting page ids')
    page_ids = get_page_ids(vc, book_id)

    if not page_ids:
        print('no pages exist')
        return

    def read_page():
        delimiter_regex = r'^===PAGE[^\n=]*===\n$'
        lines = []
        while True:
            line = lines_stream.readline()
            if not line:
                return lines, True
            if re.match(delimiter_regex, line, re.IGNORECASE):
                return lines, False
            if str(line).startswith('#'):
                continue
            lines.append(line)

    batch_size = 10
    no_batches = len(page_ids) // batch_size + 1

    for batch_count in range(no_batches):
        batch_page_ids = page_ids[batch_count * batch_size: (batch_count + 1) * batch_size]
        if not len(batch_page_ids):
            break

        pages_lines = []
        exhausted = False
        for i in range(batch_size):
            page_lines, exhausted = read_page()
            pages_lines.append(page_lines)
            if exhausted:
                break

        associations_graph = get_associations_graph(vc, batch_page_ids, pages_lines)
        print('posting batch {}'.format(batch_count))
        #  print(associations_graph)
        # noinspection PyUnusedLocal
        resp = objstore.post_graph(vc, associations_graph, should_return_resources=False, upsert=True)

        if exhausted:
            break


if __name__ == '__main__':
    if len(sys.argv) < 6:
        print("Usage: {} <site_url> <email> <password> <book_id> <source_file>".format(fname))
        sys.exit(1)

    site_url = sys.argv[1]
    email = sys.argv[2]
    password = sys.argv[3]

    _book_id = sys.argv[4]
    source_file = sys.argv[5]

    source_stream = open(source_file)
    _vc = VedavaapiSession(site_url)
    _vc.signin(email, password)

    post_associations(_vc, _book_id, source_stream)
