#!/usr/bin/python
import sys
import os

from vedavaapi.client.iiif_import_helper import IIIFImporter

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

from vedavaapi.client import *

if len(sys.argv) < 3:
    print("Usage: {} <site_url> <email> <password>".format(fname))
    sys.exit(1)

site_url = sys.argv[1]
email = sys.argv[2]
password = sys.argv[3]
library_id = sys.argv[4] if len(sys.argv) > 4 else '0'
vsession = VedavaapiSession(site_url)

vsession.signin(email, password)


def check_known_namespaces(url: str):
    if url.startswith('https://archive.org/') or url.startswith('https://iiif.archivelab.org'):
        return 'archive.org'
    return None


while True:
    try:
        iiif_url = input("Enter URL of a book from Archive.org: ")
        if not iiif_url:
            sys.exit(0)
    except Exception as e:
        sys.exit(0)
    iiif_importer = IIIFImporter.get_importer(check_known_namespaces(iiif_url))
    iiif_importer.import_from_book_url(vsession, iiif_url, library_id)
