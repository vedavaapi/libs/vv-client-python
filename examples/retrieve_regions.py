import json
import os
import re
import sys

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, os.path.join(app_path, ".."))

from requests import HTTPError
from vedavaapi.client import VedavaapiSession, objstore
from vedavaapi.client import eprint


def get_book_region_annotations(vsession: VedavaapiSession, book_id):
    try:
        graph_resp = objstore.get_graph(
            vsession,  # VedavaapiSession
            {"_id": book_id},  # Start nodes selector
            [{"source": {"jsonClass": 
                            { "$in" : 
                                ["ScannedPage", "ImageRegion"] } },
              "target": {"jsonClass": "TextAnnotation" } }],
            include_incomplete_paths=True, direction='referrer', max_hops=-1,
            json_class_projection_map={"*": {"resolvedPermissions": 0}}
        )
    except HTTPError as e:
        eprint('error in retrieving graph')
        return None

    graph = graph_resp['graph']
    #  dot_graph = graph_resp['dot_graph']

    if book_id not in graph:
        eprint('book not found')
        return None

    book = graph[book_id]
    res = { 'book' : book, 'pages' : [] }
    for page_id in book['_reached_ids'].get('source', []):
        page = graph[page_id]
        page_image_id = re.match(r'_OOLD:(.*)$', page['representations']['stillImage'][0]['data']).group(1)

        reached_ids = page['_reached_ids']
        reached_ids_through_source = reached_ids.get('source', [])

        region_annotations = []
        for region_id in reached_ids_through_source:
            region = graph.get(region_id)
            if not region:
                continue
            fragment = region['selector']['default']['value']
            xywh = re.match(r'xywh\s*=\s*(?P<xywh>.*)$', fragment).group('xywh')
            anno_ids = region.get('_reached_ids').get('target', [])
            annos = [graph.get(anno_id) for anno_id in anno_ids if anno_id in graph]
            region_annotations.append({
            #    "region": region,
                "annos": annos,
                "region_url": os.path.join(
                    vsession.base_url, 'iiif_image/v1/objstore/{file_id}/{xywh}/full/0/default.jpg'.format(
                        file_id=page_image_id, xywh=xywh)
                )
            })
        res['pages'].append({"page" : page, 
                "region_annotations": region_annotations})

    return res


if __name__ == '__main__':

    (file_path, fname) = os.path.split(__file__)
    app_path = file_path
    if app_path:
        os.chdir(app_path)
    sys.path.insert(0, os.path.join(app_path, ".."))

    from vedavaapi.client import VedavaapiSession

    if len(sys.argv) < 3:
        eprint("Usage: {} <site_url> <email> <password> <book_id>".format(fname))
        sys.exit(1)

    site_url = sys.argv[1]
    email = sys.argv[2]
    password = sys.argv[3]
    book_id = sys.argv[4]
    #file_to_save = sys.argv[5] if len(sys.argv) > 5 else 'region_annos.json'

    vsession = VedavaapiSession(site_url)
    vsession.signin(email, password)

    book_region_annotations = get_book_region_annotations(vsession, book_id)
    print(json.dumps(book_region_annotations, indent=2, ensure_ascii=False))
    #open(file_to_save, 'wb').write(json.dumps(page_region_annotations_list, indent=2, ensure_ascii=False).encode('utf-8'))
